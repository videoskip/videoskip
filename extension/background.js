function secondsToMS (totalSeconds) {
  let minutes = Math.floor(totalSeconds / 60)
  let seconds = Number((totalSeconds - (minutes * 60)).toFixed(3))

  if (minutes < 10) {
    minutes = '0' + minutes
  }

  if (seconds < 10) {
    seconds = '0' + seconds
  }

  return minutes + ':' + seconds
}

function notifyTimeChange (from, to) {
  chrome.notifications.create('skipnotif', {
    type: 'basic',
    title: 'VideoSkip',
    message: `Skipping from ${secondsToMS(from)} to ${secondsToMS(to)}.`,
    iconUrl: 'icons/logo.svg'
  })
}

chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
  switch (request.type) {
    case 'time change': {
      notifyTimeChange(request.from, request.to)
      break
    }
    // CORB
    case 'video': {
      fetch(`https://videoskip.org/api/videos/?service=${request.service}&video_id=${request.video}`)
        .then(response => response.json())
        .then(json => {
          if (json.results.length === 0) {
            console.log('Video not in database.')
            sendResponse(null)
          } else {
            sendResponse(json.results[0])
          }
        })
        .catch(console.error)
      return true // respond asynchronously
    }
    case 'service': {
      fetch(`https://videoskip.org/api/services/?service_id=${request.service}`)
        .then(response => response.json())
        .then(json => {
          if (json.length === 0) {
            console.log('Service not in database.')
            sendResponse(null)
          } else {
            sendResponse(json[0])
          }
        })
        .catch(console.error)
      return true // respond asynchronously
    }
    case 'badge': {
      console.log('badge: ', request.text, sender)
      chrome.browserAction.setBadgeText({
        text: request.text,
        tabId: sender.tab.id
      })
    }
  }
})

chrome.browserAction.setBadgeBackgroundColor({
  color: '#808080'
})
if (chrome.browserAction.setBadgeTextColor) {
  chrome.browserAction.setBadgeTextColor({
    color: '#ffffff'
  })
}

// when page changes
chrome.tabs.onUpdated.addListener((tabId, changeInfo, tabInfo) => {
  if (changeInfo.status === 'complete') {
    // console.log('page changed')
    chrome.tabs.sendMessage(tabId, {
      type: 'update page'
    })
  }
})

// first time load (mainly for addon development)
chrome.tabs.query({
  active: true,
  currentWindow: true
}, tabs => {
  if (chrome.runtime.lastError) {
    console.log(chrome.runtime.lastError.message)
  } else {
    chrome.tabs.sendMessage(tabs[0].id, {
      type: 'update page'
    })
  }
})
