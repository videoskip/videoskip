class Popup {
  constructor () {
    this.timestampTable = document.getElementById('timestampTable')
    this.timestampBody = document.getElementById('timestampBody')
    this.serviceNotSupported = document.getElementById('serviceNotSupported')
    this.noVideo = document.getElementById('noVideo')
    this.video = document.getElementById('video')
    this.service = document.getElementById('service')
    this.addUrl = document.getElementById('addUrl')
    this.suggestions = document.getElementById('suggestions')
    this.editUrl = document.getElementById('editUrl')

    this.timeTypes = []
  }

  secondsToMS (totalSeconds) {
    let minutes = Math.floor(totalSeconds / 60)
    let seconds = Number((totalSeconds - (minutes * 60)).toFixed(3))

    if (minutes < 10) {
      minutes = '0' + minutes
    }

    if (seconds < 10) {
      seconds = '0' + seconds
    }

    return minutes + ':' + seconds
  }

  updateData (message) {
    this.reset()
    this.setService(message.service)
    this.setVideo(message.service, message.video, message.timestamps)

    const timestamps = message.timestamps.map((timestamp, index) => {
      if (timestamp.time_type) {
        timestamp.time_type = {
          name: timestamp.time_type,
          description: 'Fetching...'
        }

        fetch(timestamp.time_type.name)
          .then(res => res.json())
          .then(json => {
            this.setTimeType(json, index)
          }).catch(console.error)
      } else {
        timestamp.time_type = {
          name: 'Other',
          description: 'Uncategorized time type'
        }
      }

      timestamp.start = this.secondsToMS(timestamp.start)
      timestamp.end = this.secondsToMS(timestamp.end)

      return timestamp
    })

    this.setTimestamps(timestamps)
  }

  reset () {
    this.serviceNotSupported.classList.remove('hidden')
    this.noVideo.classList.add('hidden')
    this.suggestions.classList.add('hidden')
    this.timestampTable.classList.add('hidden')

    while (this.timestampBody.firstChild) {
      this.timestampBody.removeChild(this.timestampBody.firstChild)
    }

    this.timeTypes = []
  }

  setService (service) {
    if (service) {
      this.serviceNotSupported.classList.add('hidden')
    }
  }

  setVideo (service, video, timestamps) {
    if (!service) {
      return
    } else if (service && video && timestamps.length === 0) {
      this.noVideo.classList.remove('hidden')
      this.video.innerText = video
      this.service.innerText = service.name
    } else if (service && video && timestamps.length !== 0) {
      this.suggestions.classList.remove('hidden')
    }
    // TODO make service id better
    const url = `https://videoskip.org/find-video?service=${service.id - 1}&video=${video}`
    this.addUrl.setAttribute('href', url)
    this.editUrl.setAttribute('href', url)
  }

  setTimestamps (timestamps) {
    if (timestamps.length === 0) {
      return
    }

    this.timestampTable.classList.remove('hidden')

    for (const timestamp of timestamps) {
      const tr = document.createElement('tr')

      const type = document.createElement('td')
      const start = document.createElement('td')
      const end = document.createElement('td')

      type.innerText = timestamp.time_type.name
      type.title = timestamp.time_type.description
      start.innerText = timestamp.start
      end.innerText = timestamp.end

      tr.appendChild(type)
      tr.appendChild(start)
      tr.appendChild(end)

      this.timeTypes.push(type)
      this.timestampBody.appendChild(tr)
    }
  }

  setTimeType (timeType, index) {
    this.timeTypes[index].innerText = timeType.name
    this.timeTypes[index].title = timeType.description
  }
}

var popup = new Popup()

chrome.tabs.query({
  active: true,
  currentWindow: true
}, tabs => {
  chrome.tabs.sendMessage(tabs[0].id, {
    type: 'request info'
  }, response => {
    if (chrome.runtime.lastError) {
      console.log(chrome.runtime.lastError.message)
    } else {
      // console.log(`Message from the background script: ${JSON.stringify(response)}`)
      if (response) {
        popup.updateData(response)
      }
    }
  })
})

chrome.runtime.onMessage.addListener(request => {
  // console.log('Message from the content script: ' + JSON.stringify(request))
  switch (request.type) {
    case 'info': {
      popup.updateData(request)
      break
    }
  }
})
