class VideoSkip {
  setService () {
    switch (location.hostname) {
      // TODO add other localized urls?
      case 'www.youtube.com': {
        const match = location.href.match(/v=(.{11})/)
        if (!match) {
          return false
        }
        this.selector = 'video.html5-main-video'
        this.videoId = match[1]
        this.serviceId = 'youtube'
        break
      }
    }
  }

  run () {
    // the background script manages how the content script is used
    chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
      // console.log('Message from the popup script: ' + JSON.stringify(request))
      switch (request.type) {
        case 'request info': {
          this.sendUpdatedInfo(sendResponse)
          break
        }
        case 'update page': {
          this.getUpdatedInfo()
          break
        }
      }
    })
  }

  reset () {
    this.selector = null
    this.videoId = null
    this.serviceId = null
    this.timestamps = []
    this.video = null
    this.service = null
  }

  getUpdatedInfo () {
    this.reset()
    this.setService()

    // console.log(this.serviceId, this.videoId)

    if (!this.serviceId) {
      this.updateBadge()
      return
    }

    // check with api
    chrome.runtime.sendMessage({
      type: 'service',
      service: this.serviceId
    }, service => {
      if (!service) {
        this.updateBadge()
        return
      }

      this.service = service

      this.getSkipTimes(video => {
        if (!video) {
          this.updateBadge()
          return
        }

        this.timestamps = video.timestamps
        this.updateBadge()
        this.sendUpdatedInfo(chrome.runtime.sendMessage)

        this.runInterval(this.timestamps)
      })
    })
  }

  updateBadge () {
    // console.log('page info: ', this.timestamps, this.service)

    let text = ''

    if (this.timestamps) {
      text = String(this.timestamps.length)
    } else if (this.service) {
      text = '0'
    }

    chrome.runtime.sendMessage({
      type: 'badge',
      text: text
    })
  }

  sendUpdatedInfo (sendResponse) {
    // console.log('sending update...')
    sendResponse({
      type: 'info',
      video: this.videoId,
      service: this.service,
      timestamps: this.timestamps || []
    })
  }

  // Call to external API to get video skip times
  getSkipTimes (cb) {
    chrome.runtime.sendMessage({
      type: 'video',
      service: this.service.id,
      video: this.videoId
    }, cb)
  }

  notifyTimeChange (from, to) {
    chrome.runtime.sendMessage({
      type: 'time change',
      from: from,
      to: to
    })
  }

  runInterval (times) {
    const video = document.querySelector(this.selector)
    // console.log(video)

    // video.pause()

    video.ontimeupdate = () => {
      // console.log(video.currentTime)

      for (const i in times) {
        const time = times[i]
        // console.log(time, video.currentTime, video.duration)

        // Check if the current time is less than the duration.
        // This avoids infinite reloading of the video.
        if (video.currentTime > time.start && video.currentTime < time.end && video.currentTime < video.duration - 1) {
          let end = time.end
          // Check if the time.end is less than the video duration.
          // This avoids starting the video over again.
          // (This does not update time.end in case the first video is an ad video)
          if (end > video.duration) {
            end = video.duration
          }
          this.notifyTimeChange(video.currentTime, end)
          video.currentTime = end
        }
      }
    }
  }
}

var videoSkip = new VideoSkip()
videoSkip.run()
