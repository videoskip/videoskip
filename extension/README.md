# VideoSkip Extension

## Development

Install web-ext

```sh
yarn run dev
```

## Building

```sh
yarn sign
yarn build
```

The logo must be in png format for chromium based browsers. Otherwise, the logo can be a "Plain SVG".