from django.db.models.signals import pre_delete
from django.dispatch import receiver

from . import models


@receiver(pre_delete, sender=models.TimestampRevision)
def delete_timestamp_revision(sender, instance, **kwargs):
    # Set video revision to prev
    if instance.previous_revision and hasattr(instance, 'video'):
        instance.video.timestamp_revision = instance.previous_revision
    # Set next.prev -> prev
    if instance.previous_revision and hasattr(instance, 'next_revision'):
        instance.next_revision.previous_revision = instance.previous_revision
