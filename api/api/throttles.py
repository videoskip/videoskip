from rest_framework import throttling
from rest_framework.permissions import SAFE_METHODS


class VideoSkipThrottle(throttling.UserRateThrottle):
    """
    Only rate limit unsafe methods if the user is not an official bot.
    """
    def allow_request(self, request, view):
        if (request.method in SAFE_METHODS or
                request.user.groups.filter(name='Bot').exists()):
            return True
        return super().allow_request(request, view)
