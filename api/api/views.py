from django.contrib.admin.models import DELETION, LogEntry, CHANGE, ADDITION
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.utils.timezone import now
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.exceptions import ValidationError, PermissionDenied
from rest_framework.response import Response

from . import filters
from . import models
from . import permissions
from . import serializers


class LogDestroyMixin:

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()

        LogEntry.objects.log_action(
            user_id=request.user.id,
            content_type_id=ContentType.objects.get_for_model(instance).pk,
            object_id=instance.id,
            object_repr=repr(instance),
            action_flag=DELETION)

        return super().destroy(request, *args, **kwargs)


class ServiceViewSet(LogDestroyMixin, viewsets.ModelViewSet):
    """The service used for each video.

    Clients can either retrieve this page, or utilize the filter to check if a
    service exists.
    In either case, clients should cache the response.

    NOTE: Creating services requires moderator approval!
    """
    queryset = models.Service.objects.all()
    serializer_class = serializers.ServiceSerializer
    filter_fields = ('service_id', 'name', 'user')
    permission_classes = (permissions.ModifyAllButDeleteByOwner,)
    # If we ever have too many services (>50) we can think about adding back
    # pagination
    pagination_class = None


class TimestampRevisionViewSet(LogDestroyMixin, viewsets.ModelViewSet):
    """Timestamp revisions

    Clients can used this to grab all revisions for a video.
    """
    queryset = models.TimestampRevision.objects.all()
    serializer_class = serializers.TimestampRevisionSerializer
    filter_fields = ('video', 'user')
    permission_classes = (permissions.ReadOnlyAndDeleteByOwner,)
    # Typical use case is to show the most recent revision first, so set that
    # as the default ordering
    ordering = ('-id',)


class TimeTypeViewSet(LogDestroyMixin, viewsets.ModelViewSet):
    """Time types that can be used for each timestamp. Null means "Other."

    Writing clients should call this page in order to provide users a list of
    available time types to choose from.
    Clients should cache time types as they are requested.

    NOTE: Creating time types requires moderator approval!
    """
    queryset = models.TimeType.objects.all()
    serializer_class = serializers.TimeTypeSerializer
    filter_fields = ('name', 'user')
    permission_classes = (permissions.ModifyAllButDeleteByOwner,)
    # If we ever have too many time types (>50) we can think about adding back
    # pagination
    pagination_class = None


class VideoViewSet(LogDestroyMixin, viewsets.ModelViewSet):
    """Videos

    Clients should utilize the filter based on service and video_id.
    For example, when a user lands on a page:
    1. Check if the service exists by filtering the service endpoint.
    2. If the service endpoint returns a valid service, use the service's url
    key along with the video_id to check if a video exists.

    When sending a PUT or PATCH request, the timestamp ID is not used/required.
    """
    queryset = models.Video.objects.all()
    serializer_class = serializers.VideoSerializer
    filter_fields = ('video_id', 'service', 'user')
    permission_classes = (permissions.ModifyAllButDeleteByOwner,)

    @action(detail=False, methods=['get'])
    def count(self, request):
        count = self.queryset.count()
        return Response({'count': count})


class UserViewSet(viewsets.ModelViewSet):
    """User

    Can be used to filter the username to get their id, which can then be
    used to filter other endpoints.
    """
    queryset = User.objects.all()
    serializer_class = serializers.UserSerializer
    filter_fields = ('username',)
    permission_classes = (permissions.ModeratorActionPermission,)

    @action(detail=False, methods=['get'])
    def me(self, request):
        return Response(serializers.UserSerializer(
            request.user, context=self.get_serializer_context()).data)

    @action(detail=True, methods=['delete'])
    def ban(self, request, pk=None):
        user = self.get_object()

        LogEntry.objects.log_action(
            user_id=request.user.id,
            content_type_id=ContentType.objects.get_for_model(user).pk,
            object_id=user.id,
            object_repr=repr(user),
            action_flag=DELETION)

        user.is_active = False
        user.save()

        return Response(serializers.UserSerializer(
            user, context=self.get_serializer_context()).data)


class ModQueueViewSet(viewsets.ModelViewSet):
    """The mod queue"""
    queryset = models.ModQueue.objects.all()
    serializer_class = serializers.ModQueueSerializer
    filterset_class = filters.ModQueueFilter
    permission_classes = (permissions.ModQueuePermissions,)
    # content_object errors out
    ordering_fields = (
        'id', 'url', 'content_type', 'object_id', 'request', 'request_type',
        'submitted_by', 'submitted_on', 'approved', 'approved_by',
        'approved_on', 'reason')

    def get_queryset(self):
        user = self.request.user
        if not user.is_authenticated:
            raise PermissionDenied()
        if user.groups.filter(name='Moderator').exists():
            return self.queryset
        return models.ModQueue.objects.filter(submitted_by=user)

    @action(detail=True, methods=['post'])
    def approve(self, request, pk=None):
        return self._approval(request, True)

    @action(detail=True, methods=['post'])
    def disapprove(self, request, pk=None):
        return self._approval(request, False)

    def _approval(self, request, approved):
        serializer = serializers.ApprovalSerializer(data=request.data)
        serializer.is_valid()
        data = serializer.validated_data

        instance = self.get_object()
        instance.approved_by = request.user
        instance.approved_on = now()

        if 'reason' in data:
            instance.reason = data['reason']

        if not approved:
            instance.save()
            return Response(serializers.ModQueueSerializer(
                instance, context=self.get_serializer_context()).data)

        content_object = instance.content_object
        if content_object and instance.request_type == DELETION:
            content_object.delete()
        elif content_object and instance.request_type == CHANGE:
            for key in instance.request:
                setattr(content_object, key, instance.request[key])
            content_object.save()
        elif instance.request_type == ADDITION:
            instance.content_type.model_class().objects.create(
                user=instance.submitted_by,
                **instance.request)
        else:
            raise ValidationError('content object was null, but was not a '
                                  'addition request. This may be a bug.')

        # save after setting content object in case of error
        instance.approved = True
        instance.save()
        return Response(serializers.ModQueueSerializer(
            instance, context=self.get_serializer_context()).data)
