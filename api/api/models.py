from django.contrib.admin.models import ACTION_FLAG_CHOICES
from django.contrib.auth.models import User
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.contrib.postgres.fields import JSONField
from django.core.exceptions import ValidationError
from django.db import models


class Service(models.Model):
    """Video's Service

    The service/website. For example, YouTube.
    The name would be YouTube.
    The service_id would be youtube.
    """
    user = models.ForeignKey(
        User, null=True, blank=False, on_delete=models.SET_NULL,
        related_name='services',
        help_text='User who created the service')
    service_id = models.CharField(
        max_length=64, unique=True,
        help_text='The programmatic name of the service.')
    name = models.CharField(
        max_length=64, unique=True,
        help_text='The user facing name of the service.')
    description = models.CharField(
        max_length=512,
        help_text='The description of the service.')
    modified = models.DateTimeField(
        auto_now=True,
        help_text='When this was last modified')

    def __str__(self):
        return f'{self.pk}. {self.name}'


class TimeType(models.Model):
    """Time Type (intro, outro, etc)"""
    user = models.ForeignKey(
        User, null=True, blank=False, on_delete=models.SET_NULL,
        related_name='time_types',
        help_text='User who created the time type')
    name = models.CharField(
        max_length=32, unique=True,
        help_text='The user facing type\'s name.')
    description = models.CharField(
        max_length=512,
        help_text='The type\'s description.')
    modified = models.DateTimeField(
        auto_now=True,
        help_text='When this was last modified')

    def __str__(self):
        return f'{self.pk}. {self.name}'


class TimestampRevision(models.Model):
    """Timestamp Revision

    This is meant to be a simple, linear, revision system.
    Videos cannot be reverted to an older revision, only new revisions can be
    added. This is in place to allow users to submit timestamp changes,
    while also being able to detect malicious revisions.
    The malicious (or mistaken) timestamp revisions can then be deleted,
    and good timestamp revisions restored.
    """
    # null in case the user is deleted.
    user = models.ForeignKey(
        User, null=True, blank=False, on_delete=models.SET_NULL,
        related_name='timestamp_revisions',
        help_text='User who created the revision')
    created = models.DateTimeField(
        auto_now_add=True,
        help_text='When this patchset was completed')
    # null is the 1st revision
    previous_revision = models.OneToOneField(
        'self', null=True, blank=True, on_delete=models.SET_NULL,
        related_name='next_revision',
        help_text='The service the video is from.')
    # This should never be null, but we need either video.timestamp_revision or
    # this to be null since both are created at the same time. Since video
    # really should always have a timestamp revision, I chose this to be
    # possibly null. At the moment, this is only informational anyways.
    video = models.ForeignKey(
        'Video', null=True, blank=True, on_delete=models.CASCADE,
        related_name='timestamp_revisions',
        help_text='The video this revision is meant for')

    def __str__(self):
        next_revision = video = ''
        if self.previous_revision:
            previous_revision = f'Previous: {self.previous_revision.pk}'
        else:
            previous_revision = 'Init'
        if self.video:
            next_revision = f' [Video: {self.video}]'
        if hasattr(self, 'next_revision'):
            next_revision = f', Next: {self.next_revision.pk}'

        return f'{self.pk}. {previous_revision}{next_revision}{video}'


class Video(models.Model):
    """The video"""
    user = models.ForeignKey(
        User, null=True, blank=False, on_delete=models.SET_NULL,
        related_name='videos',
        help_text='User who created the video')
    video_id = models.CharField(
        max_length=64, db_index=True,
        help_text='The video id from the service.',)
    service = models.ForeignKey(
        Service, on_delete=models.CASCADE, related_name='videos',
        help_text='The service the video is from.')
    timestamp_revision = models.OneToOneField(
        TimestampRevision, on_delete=models.CASCADE, related_name='+',
        help_text='Points to the latest version of the timestamps')

    def __str__(self):
        service = '[No service]' if not self.service else self.service.name
        return f'{self.pk}. {service} {self.video_id}'


class Timestamp(models.Model):
    """Timestamp"""
    start = models.FloatField(
        help_text='The start of the timestamp')
    end = models.FloatField(
        help_text='The end of the timestamp')
    # Null means "Other"
    time_type = models.ForeignKey(
        TimeType, on_delete=models.SET_NULL, related_name='timestamps',
        null=True, blank=True,
        help_text='The type of the timestamp')
    timestamp_revision = models.ForeignKey(
        TimestampRevision, on_delete=models.CASCADE, related_name='timestamps',
        help_text='The revision that this timestamp is for')

    def __str__(self):
        time_type = 'Other' if not self.time_type else self.time_type.name
        return f'{self.pk}. {time_type} {self.start}-{self.end}'


class ModQueue(models.Model):
    content_type = models.ForeignKey(
        ContentType, on_delete=models.CASCADE,
        help_text='The model the request is for.')
    object_id = models.PositiveIntegerField(
        null=True, blank=True,
        help_text='Optional field for CHANGE and DELETE requests.'
                  'If null, then it is meant for an ADD request.')
    content_object = GenericForeignKey()
    request = JSONField(
        help_text='The json body of the request')
    request_type = models.PositiveSmallIntegerField(
        blank=False, choices=ACTION_FLAG_CHOICES,
        help_text='The type of request to make.')
    submitted_by = models.ForeignKey(
        User, null=True, blank=False, on_delete=models.SET_NULL,
        related_name='queued',
        help_text='The user who created the request')
    submitted_on = models.DateTimeField(
        auto_now_add=True,
        help_text='When this request was created')
    approved = models.BooleanField(
        default=False,
        help_text='Whether the request has been approved')
    # If this is is set, and approved is false, then that means that a moderator
    # disapproved of a change.
    # Having this set removes it from the queue.
    approved_by = models.ForeignKey(
        User, null=True, blank=True, on_delete=models.SET_NULL,
        related_name='approved',
        help_text='User who approved the request')
    # This can be used to compare the modified date of the object requested to
    # be changed.
    approved_on = models.DateTimeField(
        null=True, blank=True,
        help_text='When this request was approved')
    reason = models.CharField(
        blank=True, max_length=1024,
        help_text='The reason why the request was approved')

    def clean(self):
        if self.request_type == '':
            raise ValidationError('Empty request_type')

    def __str__(self):
        if not self.approved_on:
            approved = 'Awaiting approval'
        elif self.approved:
            approved = 'Approved'
        else:
            approved = 'Not approved'
        return f'{self.pk}. {self.request_type} {approved}'
