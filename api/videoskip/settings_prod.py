"""This is the settings for use in production"""
from .settings_base import *

BASE_CLIENT_URL = os.environ['CLIENT_URL'].split('//')[1]

ALLOWED_HOSTS = ['localhost', '127.0.0.1', '0.0.0.0', BASE_CLIENT_URL]

SECRET_KEY = os.environ['SECRET_KEY']

DEBUG = False

ADMINS = [('', os.environ['ADMIN_EMAIL'])]

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'postgres',
        'USER': 'postgres',
        'PASSWORD': os.environ['POSTGRES_PASSWORD'],
        'HOST': 'db',
        'PORT': 5432,
    }
}

# You need to replace "example.com" in the sites page with your site.
SITE_ID = 1

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
        },
    },
    'loggers': {
        'django': {
            'handlers': ['console'],
            'level': 'INFO',
            'propagate': True,
        },
    },
}

REST_FRAMEWORK['DEFAULT_THROTTLE_CLASSES'] = (
    'api.throttles.VideoSkipThrottle',
)
REST_FRAMEWORK['DEFAULT_THROTTLE_RATES'] = {
    # only allow 1 unsafe method usage (VideoSkipThrottle) every 10 seconds.
    'user': '6/minute',
}

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = os.environ['EMAIL_HOST']
EMAIL_HOST_USER = os.environ['EMAIL_USER']
EMAIL_HOST_PASSWORD = os.environ['EMAIL_PASSWORD']
EMAIL_USE_TLS = True

CLIENT_URL = os.environ['CLIENT_URL']

LOGIN_REDIRECT_URL = CLIENT_URL + '/profile'

DEFAULT_FROM_EMAIL = f'admin@{BASE_CLIENT_URL}'
