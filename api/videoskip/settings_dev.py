"""This is the default development settings"""
from .settings_base import *

ALLOWED_HOSTS = ["localhost", "127.0.0.1", "0.0.0.0"]

SECRET_KEY = 'secret'

DEBUG = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'postgres',
        'USER': 'postgres',
        'HOST': 'localhost',
        'PORT': 5432,
    }
}

SITE_ID = 1

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

# The client url
CLIENT_URL = 'http://localhost:3000'

# Redirect successful user register confirmation
LOGIN_REDIRECT_URL = CLIENT_URL + '/profile'

# Used to test throttling
# REST_FRAMEWORK['DEFAULT_THROTTLE_CLASSES'] = (
#     'api.throttles.VideoSkipThrottle',
# )
# REST_FRAMEWORK['DEFAULT_THROTTLE_RATES'] = {
#     'user': '1/minute',
# }
