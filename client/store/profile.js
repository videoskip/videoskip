// This module is persisted. See plugins/vuex-persist.
export const state = () => ({
  apiKey: ''
})

export const mutations = {
  set (state, data) {
    state.apiKey = data.key
  }
}
