#!/bin/bash
# Expected to be run in root directory

set -eo pipefail

rm -f api/TEST-api.tests.*
rm -rf api/htmlcov
