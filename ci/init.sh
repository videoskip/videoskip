#!/bin/bash
# Expected to be run in root directory

set -exo pipefail

cd api
pipenv install --dev

cd ../client
yarn install

cd ../extension
yarn install
